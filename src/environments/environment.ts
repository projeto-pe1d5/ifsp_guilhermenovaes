// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyDuvNhIzD9Hj7F1mSCy4j9GPMJlHMZ8Yr4',
    authDomain: 'controle-3de11.firebaseapp.com',
    databaseURL: 'https://controle-3de11.firebaseio.com',
    projectId: 'controle-3de11',
    storageBucket: 'controle-3de11.appspot.com',
    messagingSenderId: '310794400103',
    appId: '1:310794400103:web:4e00ab4d254dfc22b4bfc2',
    measurementId: 'G-2J4HL10WNE'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
