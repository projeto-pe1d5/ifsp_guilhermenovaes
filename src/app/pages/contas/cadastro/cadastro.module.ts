import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CadastroPage } from './cadastro.page';
import { PagarPageRoutingModule } from '../pagar/pagar-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PagarPageRoutingModule,
  ],
  declarations: [CadastroPage]
})
export class CadastroPageModule {}
