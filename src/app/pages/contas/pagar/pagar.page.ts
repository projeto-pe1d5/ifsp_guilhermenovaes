import { ContaService } from './../service/conta-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})

export class PagarPage implements OnInit {

  listaContas;

  constructor(
    private conta: ContaService
  ) { }

  ngOnInit() {
    this.conta.listar('pagar')
          .subscribe( x => this.listaContas = x);
  }

  remover(conta){
    this.conta.remover(conta);
  }
}